from django.forms import ModelForm, DateTimeInput
from tasks.models import Task

dateTimeInput = DateTimeInput(
    attrs={
        "placeholder": "Select a date",
        "type": "datetime-local",
    },
)

# <input type="datetime-local" placeholder="Select a date" />


class TaskForm(ModelForm):
    class Meta:
        model = Task
        fields = [
            "name",
            "start_date",
            "due_date",
            "project",
            "assignee",
        ]
        widgets = {
            "start_date": dateTimeInput,
            "due_date": dateTimeInput,
        }
